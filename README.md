Installation Guide (Frontend):
1. Clone the project from the remote Git repository
89
$ chmod +X ./setup.sh
$ ./setup.sh
$ docker-compose up --build
Figure 21. Git clone the project(front-end)
2. Change directory to the newly cloned folder and run the yarn install to install all the
npm dependencies required to run the vue application.
Figure 22. Project setup and package install
5.3 How to Operate the system
A. How to start the system
A.1 How to start the backend system (microservices)
After all services’ Docker images have been built locally, run the following command to
get the overall system up and running:
90
$ git clone https://gitlab.com/masterlanceteam/masterlance.git
$ cd masterlance-frontend/
$ yarn install
$ docker-compose up
Figure 23. Startup docker services
A.2 How to start the front-end application
Having the backend system up and running, you can now run the front-end application
with the command below:
Figure 24. Run the project(frontend)
Once successfully started the output should resemble the following:
From here forth, we will refer to the URL the app listens on (http://localhost:8080 in the above
example) as “APP_URL” for brevity and consistency.
91
$ export API_BASE_URL="http://localhost:3000" && yarn dev
B. How to create user account
Navigate to http://APP_URL/signup
and fill in the details required there, which includes:
● First Name & Last Name
● Email Address
● Address (City)
● Phone Number & Birth Date
● Username and Password
92
Figure 25. Signup page
C. Post a Job
In order to post a job, one must log in using his/her Client Profile. Inside the popup that arises
when you click on the avatar at the header, make sure it reads “client” under your full name (as
in the example found below)
Figure 26. Settings and Logout option
If the above isn’t true in your case, you must be signed in using a freelancer profile and should
be able to find a button there that says:
Clicking that would switch your account’s current role into a “client” role and the above
condition should hold true then.
After making sure you’re authenticated using a client-role, navigate inside your browser to the
URL http://APP_URL/post-job
93
Figure 27. Publish a new Job
Fill in the following fields with their respective values on there
● Job Title
● No of Freelancers required & Duration (amount and unit)
● Price, Body (Detailed Description) and Tags
and finally simply click on
94
to successfully publish a new job.
D. Top-up an account’s wallet balance
Clients are allowed to top up their wallets’ balance whenever they want to do so. This is to
enable them to have enough money to post jobs later on.
In order to top up a wallet account, you should navigate to settings first (click on the “settings”
menu item inside the header dropdown)
Once on the Settings screen, Select the “Get Paid” tab (on the left side of the screen).
Alternatively, you can skip over the steps mentioned above by simply navigating into the path
http://APP_URL/settings/payments
Figure 28. Balance page
Then click on the green button that reads “Top Up”
95
You should see a form where you can enter the amount you wish to add, enter the amount there
and finally click on “Pay with YenePay”.
You will then be redirected into YenePay’s check out screen where you’ll be asked to login with
your YenePay credentials. Login there and confirm the transaction.
Figure 29. YenePay prompt
96
If all goes according to the plan and your payment is successful, you should see a confirmation
notice on Masterlance once the transaction has been completed successfully (on notification
panel at the header).
Here is what the notification panel looks like once the transactions get committed successfully:
Figure 30. Transaction History
