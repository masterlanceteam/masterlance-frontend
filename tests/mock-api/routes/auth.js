const Users = require('../resources/users')

module.exports = (app) => {
  // Log in a user with a username and password
  app.post('/api/users/login', (request, response) => {
    Users.authenticate(request.body)
      .then((user) => {
        response.json(user)
      })
      .catch((error) => {
        response.status(401).json({ message: error.message })
      })
  })

  app.post('/api/users/logout', (request, response) => {
    response.json({
      success: true,
    })
  })

  // Get the user of a provided token, if valid
  app.get('/api/users/validate', (request, response) => {
    const currentUser = Users.findBy('id', request.query.access_token)

    if (!currentUser) {
      return response.status(401).json({
        message:
          'The token is either invalid or has expired. Please log in again.',
      })
    }

    response.json(currentUser)
  })

  // A simple ping for checking online status
  app.get('/api/ping', (request, response) => {
    response.send('OK')
  })
}
