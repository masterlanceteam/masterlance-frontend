const _ = require('lodash')

module.exports = {
  all: [
    {
      restrictionsLeft: 3,
      firstName: 'John',
      lastName: 'Doe',
      phoneNumber: '+251911112233',
      address: 'Addis Ababa',
      dob: '1996-01-01T00:00:00.000Z',
      authAs: 'client',
      username: 'john_doe',
      email: 'johndoe@gmail.com',
      id: '5f7f9e21c9d93e001d9faf8f',
      password: 'password',
    },
    {
      restrictionsLeft: 3,
      firstName: 'Jane',
      lastName: 'Doe',
      phoneNumber: '+251911112233',
      address: 'Addis Ababa',
      dob: '1996-01-01T00:00:00.000Z',
      authAs: 'freelancer',
      username: 'admin',
      email: 'admin@gmail.com',
      id: '5f7f9e21c9d93e001d9faf82',
      password: 'password',
      freelancerprofile: {
        restrictionsLeft: 3,
      },
    },
  ].map((user) => {
    return {
      user,
      ipAddress: '0.0.0.0',
      ttl: 1209600,
      created: '2020-01-01T13:36:30.344Z',
      userId: user.id,
      id: `valid-token-for-${user.email}`,
    }
  }),
  authenticate({ email, password }) {
    return new Promise((resolve, reject) => {
      const matchedUser = this.all.find(({ user }) => {
        return user.email === email && user.password === password
      })
      if (matchedUser) {
        resolve(this.json(matchedUser))
      } else {
        reject(new Error('Invalid user credentials.'))
      }
    })
  },
  findBy(propertyName, value) {
    const matchedUser = this.all.find((user) => user[propertyName] === value)
    return this.json(matchedUser)
  },
  json(user) {
    return user && _.omit(user, ['password'])
  },
}
