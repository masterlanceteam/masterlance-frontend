import Vue from 'vue'
import VueRouter from 'vue-router'
// https://github.com/declandewet/vue-meta
import VueMeta from 'vue-meta'
// Adds a loading bar at the top during page loads.
import NProgress from 'nprogress/nprogress'
import store from '@state/store'
import routes from './routes'

Vue.use(VueRouter)
Vue.use(VueMeta, {
  // The component option name that vue-meta looks for meta info on.
  keyName: 'page',
})

const router = new VueRouter({
  routes,
  // Use the HTML5 history API (i.e. normal-looking routes)
  // instead of routes with hashes (e.g. example.com/#/about).
  // This may require some server configuration in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  mode: 'history',
  // Simulate native-like scroll behavior when navigating to a new
  // route and using back/forward buttons.
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
})

// Before each route evaluates...
router.beforeEach((routeTo, routeFrom, next) => {
  // If this isn't an initial page load...
  if (routeFrom.name !== null) {
    // Start the route progress bar.
    NProgress.start()
  }

  // Check if auth is required on this route
  // (including nested routes).
  const authRequired = routeTo.matched.some((route) => route.meta.authRequired)
  const mustBeFreelancer = routeTo.matched.some((route) => route.meta.mustBeFreelancer)
  const mustBeClient = routeTo.matched.some((route) => route.meta.mustBeClient)
  const mustBeModerator = routeTo.matched.some((route) => route.meta.mustBeModerator)
  const hiddenFromModerators = routeTo.matched.some((route) => route.meta.hiddenFromModerators)


  // If auth isn't required for the route, just continue.
  if (!authRequired) return next()

  // If auth is required and the user is logged in...
  if (store.getters['auth/loggedIn']) {
    // Validate the local user token...
    return store.dispatch('auth/validate').then((validUser) => {
      // Then continue if the token still represents a valid user,
      // otherwise redirect to login.
      if (validUser) {
        if (hiddenFromModerators && validUser.user.authAs === 'moderator') {
          next({
            name: '401',
            params: { reason: 'This resource can NOT be accessed by moderators'}
          })
        }
        else if (mustBeModerator) {
          if (validUser.user.authAs !== 'moderator') {
            next({
              name: '401',
              params: { reason: 'This resource can only be accessed by moderators' }
            })
          } else {
            next()
          }
        }
        else if (mustBeFreelancer) {
          if (validUser.user.authAs === 'freelancer') {
            if (validUser.user.freelancerprofile) { // has already setup a profile
              // reauthenticate as client and continue
              next()
            } else {
              redirectToFreelancerProfileSetup()
            }
          } else {
            store.dispatch('auth/switchMode', { to: 'freelancer' }).then((_) => {
              if (validUser.user.freelancerprofile) {
                next()
              } else {
                redirectToFreelancerProfileSetup()
              }
            })
          }
        } else if (mustBeClient) {
          if (validUser.user.authAs === 'client') {
            if (validUser.user.clientprofile) { // has already setup a profile
              // reauthenticate as freelancer and continue
              next()
            } else {
              redirectToClientProfileSetup()
            }
          } else {
            store.dispatch('auth/switchMode', { to: 'client' }).then((_) => {
              if (validUser.user.clientprofile) {
                next()
              } else {
                redirectToClientProfileSetup()
              }
            })
          }
        } 
        else
          next()
      } else {
        redirectToLogin()
      }
    })
  }

  // If auth is required and the user is NOT currently logged in,
  // redirect to login.
  redirectToLogin()

  function redirectToFreelancerProfileSetup() {
    // Pass the original route to the login component
    next({ name: 'setup-freelancer-profile', query: { redirectFrom: routeTo.fullPath } })
  }
  
  function redirectToClientProfileSetup() {
    // Pass the original route to the login component
    next({ name: 'setup-client-profile', query: { redirectFrom: routeTo.fullPath } })
  }

  function redirectToLogin() {
    // Pass the original route to the login component
    next({ name: 'login', query: { redirectFrom: routeTo.fullPath } })
  }
})

router.beforeResolve(async (routeTo, routeFrom, next) => {
  // Create a `beforeResolve` hook, which fires whenever
  // `beforeRouteEnter` and `beforeRouteUpdate` would. This
  // allows us to ensure data is fetched even when params change,
  // but the resolved route does not. We put it in `meta` to
  // indicate that it's a hook we created, rather than part of
  // Vue Router (yet?).
  try {
    // For each matched route...
    for (const route of routeTo.matched) {
      await new Promise((resolve, reject) => {
        // If a `beforeResolve` hook is defined, call it with
        // the same arguments as the `beforeEnter` hook.
        if (route.meta && route.meta.beforeResolve) {
          route.meta.beforeResolve(routeTo, routeFrom, (...args) => {
            // If the user chose to redirect...
            if (args.length) {
              // If redirecting to the same route we're coming from...
              if (routeFrom.name === args[0].name) {
                // Complete the animation of the route progress bar.
                NProgress.done()
              }
              // Complete the redirect.
              next(...args)
              reject(new Error('Redirected'))
            } else {
              resolve()
            }
          })
        } else {
          // Otherwise, continue resolving the route.
          resolve()
        }
      })
    }
    // If a `beforeResolve` hook chose to redirect, just return.
  } catch (error) {
    return
  }

  // If we reach this point, continue resolving the route.
  next()
})

// When each route is finished evaluating...
router.afterEach((routeTo, routeFrom) => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router
