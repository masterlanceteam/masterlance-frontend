import store from '@state/store'

export default [
  {
    path: '/',
    name: 'home',
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          const { user } = store.state.auth.currentUser
          if (user.authAs === 'client') {
            if (user.clientprofile) {
              next({ name: 'client-home' })
            } else {
              next({ name: 'setup-client-profile' })
            }
          }
          else if (user.authAs === 'freelancer') {
            if (user.freelancerprofile) {
              next({ name: 'find-work' })
            } else {
              next({ name: 'setup-freelancer-profile' })
            }
          }
          else
            next({
              name: '404'
            })
        } else {
          // Continue to the login page
          next({
            name: 'landing-page'
          })
        }
      },
    },
  },
  {
    path: '/',
    name: 'landing-page',
    component: () => lazyLoadView(import('@views/home.vue'))
  },
  {
    path: '/login',
    name: 'login',
    component: () => lazyLoadView(import('@views/auth/login.vue')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({ name: 'home' })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => lazyLoadView(import('@views/auth/register.vue')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          next({ name: 'home' })
        } else {
          // Continue to the register page
          next()
        }
      },
    },
  },
  {
    path: '/moderator/dashboard',
    name: 'moderator-home',
    component: () => lazyLoadView(import('@views/Moderator/dashboard.vue')),
    meta: {
      authRequired: false,
      mustBeClient: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/setup-profile/freelancer',
    name: 'setup-freelancer-profile',
    component: () => lazyLoadView(import('@views/auth/freelancer-profile-setup.vue')),
    meta: {
      authRequired: true,
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in and has a freelancer profile already
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          const { user } = store.state.auth.currentUser
          if (user.freelancerprofile) {
            next({
              name: 'home'
            })
          }
        }
        next()
      }
    },
  },
  {
    path: '/setup-profile/client',
    name: 'setup-client-profile',
    component: () => lazyLoadView(import('@views/auth/client-profile-setup.vue')),
    meta: {
      authRequired: true,
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in and has a client profile already
        if (store.getters['auth/loggedIn']) {
          // Redirect to the home page instead
          const { user } = store.state.auth.currentUser
          if (user.clientprofile) {
            next({
              name: 'home'
            })
          }
        }
        next()
      }
    },
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => lazyLoadView(import('@views/profile.vue')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/settings',
    redirect: '/settings/contacts-info'
  },
  {
    path: '/payments/success',
    name: 'payment-success',
    component: () => lazyLoadView(import('@views/payment-notification/success.vue')),
    meta: {
      tmp: {},
      beforeResolve(routeTo, routeFrom, next) {
        routeTo.meta.tmp.transaction = routeTo.query
        next()
      },
    },
    props: (route) => ({ transaction: route.meta.tmp.transaction || {} }),
  },
  {
    path: '/settings/contacts-info',
    name: 'contacts-info',
    component: () => lazyLoadView(import('@views/settings/contacts-info.vue')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/settings/payments',
    name: 'payments',
    component: () => lazyLoadView(import('@views/settings/payments.vue')),
    meta: {
      authRequired: true,
      hiddenFromModerators: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/settings/account-security',
    name: 'account-security',
    component: () => lazyLoadView(import('@views/settings/account-security.vue')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/jobs',
    name: 'jobs',
    component: () => lazyLoadView(import('@views/job.vue')),
    meta: {
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/find-work',
    name: 'find-work',
    component: () => lazyLoadView(import('@views/Freelancer/home.vue')),
    meta: {
      authRequired: true,
      mustBeFreelancer: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/client/home',
    name: 'client-home',
    component: () => lazyLoadView(import('@views/Client/home.vue')),
    meta: {
      authRequired: true,
      mustBeClient: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/post-job',
    name: 'post-job',
    component: () => lazyLoadView(import('@views/Client/job_post.vue')),
    meta: {
      authRequired: true,
      mustBeClient: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/messages',
    name: 'messages',
    component: () => lazyLoadView(import('@views/im/chat_main.vue')),
    meta: {
      authRequired: true,
      mustBeClient: true
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/profile/:username',
    name: 'username-profile',
    component: () => lazyLoadView(import('@views/profile.vue')),
    meta: {
      authRequired: true,
      // HACK: In order to share data between the `beforeResolve` hook
      // and the `props` function, we must create an object for temporary
      // data only used during route resolution.
      tmp: {},
      beforeResolve(routeTo, routeFrom, next) {
        store
          // Try to fetch the user's information by their username
          .dispatch('users/fetchUser', { username: routeTo.params.username })
          .then((user) => {
            // Add the user to `meta.tmp`, so that it can
            // be provided as a prop.
            routeTo.meta.tmp.user = user
            // Continue to the route.
            next()
          })
          .catch(() => {
            // If a user with the provided username could not be
            // found, redirect to the 404 page.
            next({ name: '404', params: { resource: 'User' } })
          })
      },
    },
    // Set the user from the route params, once it's set in the
    // beforeResolve route guard.
    props: (route) => ({ user: route.meta.tmp.user }),
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {
      authRequired: true,
      beforeResolve(routeTo, routeFrom, next) {
        store.dispatch('auth/logOut')
        const authRequiredOnPreviousRoute = routeFrom.matched.some(
          (route) => route.meta.authRequired
        )
        // Navigate back to previous page, or home as a fallback
        next(authRequiredOnPreviousRoute ? { name: 'home' } : { ...routeFrom })
      },
    },
  },
  {
    path: '/404',
    name: '404',
    component: require('@views/_404.vue').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  {
    path: '/401',
    name: '401',
    component: require('@views/401.vue').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    redirect: '404',
  },
]

// Lazy-loads view components, but with better UX. A loading view
// will be used if the component takes a while to load, falling
// back to a timeout view in case the page fails to load. You can
// use this component to lazy-load a route with:
//
// component: () => lazyLoadView(import('@views/my-view'))
//
// NOTE: Components loaded with this strategy DO NOT have access
// to in-component guards, such as beforeRouteEnter,
// beforeRouteUpdate, and beforeRouteLeave. You must either use
// route-level guards instead or lazy-load the component directly:
//
// component: () => import('@views/my-view')
//
function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    // A component to use while the component is loading.
    loading: require('@views/_loading.vue').default,
    // Delay before showing the loading component.
    // Default: 200 (milliseconds).
    delay: 400,
    // A fallback component in case the timeout is exceeded
    // when loading the component.
    error: require('@views/_timeout.vue').default,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000,
  })

  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children)
    },
  })
}
