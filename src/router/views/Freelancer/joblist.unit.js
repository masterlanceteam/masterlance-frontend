import Joblist from './joblist'

describe('@views/joblist', () => {
  it('is a valid view', () => {
    expect(Joblist).toBeAViewComponent()
  })
})
