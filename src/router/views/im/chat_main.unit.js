import Chat_main from './chat_main'

describe('@views/im', () => {
  it('is a valid view', () => {
    expect(Chat_main).toBeAViewComponent()
  })
})
