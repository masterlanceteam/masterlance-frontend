import Job from './job'

describe('@views/job', () => {
  it('is a valid view', () => {
    expect(Job).toBeAViewComponent()
  })
})
