import axios from 'axios'

export const state = {
  availableSkills: []
}

export const mutations = {
    SET_AVAILABLE_SKILLS(state, availableSkills) {
    state.availableSkills = availableSkills
  }
}

export const getters = {
  availableSkills(state) {
    return state.availableSkills
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    // setDefaultAuthHeaders(state)
    dispatch('fetchAvailableSkills')
  },

  fetchAvailableSkills({ commit }) {
    return axios
        .get(`/api/skills/fetchAllAvailableSkills`)
        .then((response) => {
            const availableSkills = response.data
            commit('SET_AVAILABLE_SKILLS', availableSkills)
            return availableSkills
        })
        .catch(err => {
            if (err.response && err.response.status === 500) {
                const { error: {message}} = err.response.data;
                throw new Error(message)
            } else {
                throw err
            }
        })
  },

  fetchSkillsByListOfIDs({ commit }, listOfIDs) {
    return Promise.all(listOfIDs.map(id => {
      return axios
        .get(`/api/skills/skill-by-id?id=${id}`)
        .then((response) => {
          return response.data;
        })
        .catch(err => {
            if (err.response && err.response.status === 500) {
                const { error: {message}} = err.response.data;
                throw new Error(message)
            } else {
                throw err
            }
        })
    }))
  }
}
