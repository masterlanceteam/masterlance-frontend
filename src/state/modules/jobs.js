import axios from 'axios'

export const state = {
  feed: [],
  unapprovedJobs: [],
  myJobs: []
}

export const mutations = {
  SET_FEED(state, feed) {
    state.feed = feed
  },
  SET_UNAPPROVED_JOBS(state, unapprovedJobs){
    state.unapprovedJobs = unapprovedJobs
  },
  SET_MY_JOBS(state, myJobs){
    state.myJobs = myJobs
  }
}

export const getters = {
  feed(state) {
    return state.feed
  },
  unapprovedJobs(state) {
    return state.unapprovedJobs
  },
  myJobs(state) {
    return state.myJobs
  },
}
export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {// ante atekmemf
    // setDefaultAuthHeaders(state)
  },

  postJob(
    { commit, dispatch, getters, rootGetters, rootState },
    {
      title,
      price,
      description,
      noOfFreelancersNeeded,
      skillsRequired,
      duration,
    } = {}
  ) {
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
        .post(`/api/jobs/create?access_token=${currentUser.id}`, {
          title,
          description,
          duration,
          price: Number(price),
          noOfFreelancersNeeded: Number(noOfFreelancersNeeded),
          skillsRequired: skillsRequired.map(({ id }) => id),
        })
        .catch((err) => {
          if (err.response && err.response.status === 500) {
            const {
              error: { message },
            } = err.response.data
            throw new Error(message)
          } else {
            throw err
          }
        })
  },

  applyProposal(
    { commit, dispatch, getters, rootGetters, rootState },
    {
      jobId,
      price,
      body,
      duration
    } = {}
  ) {
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
        .post(`/api/jobs/${jobId}/apply?access_token=${currentUser.id}`, {
          body,
          duration: duration,
          price: Number(price),
          numberOfMilestones: 1
        })
        .catch((err) => {
          if (err.response && err.response.status === 500) {
            const {
              error: { message },
            } = err.response.data
            throw new Error(message)
          } else {
            throw err
          }
        })
  },

  // approve selected job
  approveJob({commit,  dispatch, getters, rootGetters, rootState}, { jobId }){
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
              .post(`/api/jobs/${jobId}/approve?access_token=${currentUser.id}`, { })
              .then((response) => {
                const job = response.data
                commit('REMOVE_PENDING_JOB', job)
                return job
              })
              .catch(err => {
                console.log(err)
              })
  },

  // deny approval for a selected job
  denyApproval({commit, dispatch, getters, rootGetters, rootState}, { jobId, denialMessage }){
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
              .post(`/api/jobs/${jobId}/denyApproval?access_token=${currentUser.id}`, { denialMessage })
              .then((response) => {
                const job = response.data
                commit('REMOVE_PENDING_JOB', job)
                return job
              })
              .catch(err => {
                console.log(err)
              })
  },

  haveIApplied({ commit, dispatch, getters, rootGetters, rootState }, jobId) {
    const currentUser = rootState.auth.currentUser

    if (rootGetters['auth/loggedIn'])
    return axios
      .post(`/api/jobs/${jobId}/have-i-applied?access_token=${currentUser.id}`)
      .then((response) => {
        return response.data
      })

      .catch((err) => {
        if (err.response && err.response.status === 500) {
          const {
            error: { message },
          } = err.response.data
          throw new Error(message)
        } else {
          throw err
        }
      })
  },
  fetchJobs({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser

    if (rootGetters['auth/loggedIn'])
    return axios
      .get(`/api/jobs/feed?access_token=${currentUser.id}`)
      .then((response) => {
        const feed = response.data
        commit('SET_FEED', feed)
        return feed
      })

      .catch((err) => {
        if (err.response && err.response.status === 500) {
          const {
            error: { message },
          } = err.response.data
          throw new Error(message)
        } else {
          throw err
        }
      })
  },

  fetchUnapprovedJobs ({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUsr
    return axios
      .get(`/api/jobs/fetchUnapprovedJobs?access_token=${currentUser.id}`)
      .then((response) => {
        const unapprovedJobs = response.data
        commit('SET_UNAPPROVED_JOBS', unapprovedJobs)
        return unapprovedJobs
      })
      .catch((err) => {
        if (err.response && err.response.status === 500) {
          const {
            error: { message },
          } = err.response.data
          throw new Error(message)
        } else {
          throw err
        }
      })
  },

  fetchMyJobs({ commit, dispatch, getters, rootGetters, rootState }){
    const currentUser = rootState.auth.currentUser
      return axios
      .get(`/api/jobs/my-jobs?access_token=${currentUser.id}`)
      .then((response) => {
        const myJobs = response.data
        commit('SET_MY_JOBS', myJobs)
        return myJobs
      })
      .catch((err) => {
        if (err.response && err.response.status === 500) {
          const {
            error: { message },
          } = err.response.data
          throw new Error(message)
        } else {
          throw err
        }
      })
  }
}
