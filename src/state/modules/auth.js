import axios from 'axios'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
}

export const mutations = {
  SET_CURRENT_USER(state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
    setDefaultAuthHeaders(state)
  },
}

export const getters = {
  // Whether the user is currently logged in.
  loggedIn(state) {
    return !!state.currentUser
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn({ commit, dispatch, getters }, { email, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post('/api/users/login', { email, password })
      .then((response) => {
        const user = response.data
        commit('SET_CURRENT_USER', user)
        return user
      })
  },
 
  switchMode({ state, dispatch, getters }, { to } = {}) {
    if (!getters.loggedIn) return dispatch('validate')

    return axios
      .post(`/api/users/switch-mode?access_token=${state.currentUser.id}`, { to })
      .then(() => {
        return dispatch('validate')
      })
  },
  
  createClientProfile({ state, dispatch, getters }) {
    if (!getters.loggedIn) return dispatch('validate')

    return axios
      .post(`/api/client-profiles/create?access_token=${state.currentUser.id}`)
      .then(() => {
        return dispatch('validate')
      })
  },

  createFreelancerProfile({ state, dispatch, getters }, { skillsIDs }) {
    if (!getters.loggedIn) return dispatch('validate')
    
    return axios
      .post(`/api/freelancer-profiles/create?access_token=${state.currentUser.id}`, {
        skillsIDs
      })
      .then(() => {
        return dispatch('validate')
      })
  },
 
  signUp({ commit, dispatch, getters }, {
    firstName,
    lastName,
    phoneNumber,
    address,
    dob,
    username,
    email,
    password,
    authAs
  } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post('/api/users/register', { 
        firstName,
        lastName,
        phoneNumber,
        address,
        dob,
        username,
        email,
        password,
        authAs
       })
      .then((response) => {
        // const user = response.data
        // commit('SET_CURRENT_USER', user)
        // return user
      })
  },

  // Logs out the current user.
  logOut({ commit, state }) {
    return axios
      .post(`/api/users/logout?access_token=${state.currentUser.id}`)
      .then((response) => {
        commit('SET_CURRENT_USER', null)
      })
  },

  // Validates the current user's token and refreshes it
  // with new data from the API.
  validate({ commit, state }) {
    if (!state.currentUser) return Promise.resolve(null)
    return axios
      .get(`/api/users/validate?access_token=${state.currentUser.id}`)
      .then((response) => {
        const user = response.data
        commit('SET_CURRENT_USER', user)
        return user
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          commit('SET_CURRENT_USER', null)
        } else {
          console.warn(error)
        }
        return null
      })
  },
}

// ===
// Private helpers
// ===

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState(key, state) {
  window.localStorage.setItem(key, JSON.stringify(state))
}

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.currentUser
    ? state.currentUser.token
    : ''
}
