import axios from 'axios'

export const state = {
  wallet: {
    activeBalance: null,
  },
  transactions: null
}

export const mutations = {
  SET_WALLET(state, newValue) {
    state.wallet = newValue
    setDefaultAuthHeaders(state)
  },
  SET_TRANSACTIONS(state, newValue) {
    state.transactions = newValue
    setDefaultAuthHeaders(state)
  },
  TOP_UP_TRANSACTION(state, newValue) {
    state.wallet = newValue
    setDefaultAuthHeaders(state)
  },
}

export const getters = {
  // Whether the user is currently logged in.
  balance(state) {
    return state.wallet
  },
  transactions(state) {
    return state.transactions
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    // setDefaultAuthHeaders(state)
    dispatch('fetchBalance')
    dispatch('fetchTransactions')
  },

  topUp({ commit, dispatch, getters, rootGetters, rootState }, { amount } = {}) {
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
        .post(`/api/payments/topup?access_token=${currentUser.id}`, { amount })
        .then((response) => {
            const transaction = response.data
            commit('TOP_UP_TRANSACTION', transaction)
            dispatch('fetchBalance')
            return transaction
        })
        .catch(err => {
            if (err.response && err.response.status === 500) {
              const { error: {message}} = err.response.data;
              throw new Error(message)
            } else {
              throw err
            }
        })
  },

  fetchBalance({ state, commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser
    
    if (rootGetters['auth/loggedIn'])
        return axios
            .get(`/api/payments/get-balance?access_token=${currentUser.id}`)
            .then((response) => {
                const wallet = response.data
                dispatch('fetchTransactions')
                commit('SET_WALLET', wallet)
                return wallet
            })
            .catch(err => {
              if (err.response && err.response.status === 500) {
                const { error: {message}} = err.response.data;
                throw new Error(message)
              } else {
                throw err
              }
            })
  },
  
  fetchTransactions({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser
    
    if (rootGetters['auth/loggedIn'])
        return axios
            .get(`/api/payments/transactions?access_token=${currentUser.id}`)
            .then((response) => {
                const transactions = response.data
                commit('SET_TRANSACTIONS', transactions)
                return transactions
            })
            .catch(err => {
              if (err.response && err.response.status === 500) {
                const { error: {message}} = err.response.data;
                throw new Error(message)
              } else {
                throw err
              }
            })
  },

  cancelTopupTransaction({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser
    
    if (rootGetters['auth/loggedIn'])
      return axios
        .post(`/api/payments/cancel-transaction?access_token=${currentUser.id}`)
        .then((response) => {
          dispatch('fetchBalance')
          return response
        })
        .catch(err => {
          if (err.response && err.response.status === 500) {
            const { error: {message}} = err.response.data;
            throw new Error(message)
          } else {
            throw err
          }
        })
},

  async verifyTransaction({ commit, dispatch, getters, rootGetters, rootState }, { params }) {    
    return axios
      .post(`/api/payments/verify`, {
        params
      })
      .then(() => dispatch('fetchBalance'))
      .catch(err => {
        if (err.response && err.response.status === 500) {
          const { error: {message}} = err.response.data;
          throw new Error(message)
        } else {
          throw err
        }
      })
  }
}

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.wallet
    ? state.wallet.token
    : ''
}
