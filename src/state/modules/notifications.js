import axios from 'axios'

export const state = {
  notifications: [],
}

export const mutations = {
  SET_NOTIFICATIONS(state, notifications) {
    state.notifications = notifications
  },
}

export const getters = {
  unseenNotifications(state) {
    return state.notifications.filter((notification) => !notification.visited)
  },
  notifications(state) {
    return state.notifications
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    // setDefaultAuthHeaders(state)
    dispatch('fetchNotifications')
  },

  fetchNotifications({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
        .get(`/api/notifications?access_token=${currentUser.id}`)
        .then((response) => {
          const notifications = response.data
          commit('SET_NOTIFICATIONS', notifications)
          return notifications
        })
        .catch((err) => {
          if (err.response && err.response.status === 500) {
            const {
              error: { message },
            } = err.response.data
            throw new Error(message)
          } else {
            throw err
          }
        })
  },

  markNotificationsSeen({ commit, dispatch, getters, rootGetters, rootState }) {
    const currentUser = rootState.auth.currentUser
    if (rootGetters['auth/loggedIn'])
      return axios
        .post(`/api/notifications/markVisted?access_token=${currentUser.id}`)
        .then((result) => {
          setTimeout(() => dispatch('fetchNotifications'), 500)
          return result
        })
        .catch((err) => {
          if (err.response && err.response.status === 500) {
            const {
              error: { message },
            } = err.response.data
            throw new Error(message)
          } else {
            throw err
          }
        })
  },
}
