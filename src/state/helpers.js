import { mapState, mapGetters, mapActions } from 'vuex'

export const authComputed = {
  ...mapState('auth', {
    currentUser: (state) => state.currentUser,
  }),
  ...mapGetters('auth', ['loggedIn']),
}

export const authMethods = mapActions('auth', [
  'logIn',
  'logOut',
  'signUp',
  'switchMode',
  'createClientProfile',
  'createFreelancerProfile',
])

export const jobComputed = {
  ...mapState('jobs', {
    jobs: (state) => state.jobs,
  }),
  // ...mapGetters('auth', ['loggedIn']),
}

export const jobMethods = mapActions('jobs', ['fetchPendingJobs', 'approveJob', 'denyApproval', 'fetchUnapprovedJobs'])

export const paymentsComputed = {
  ...mapState('payments', {
    wallet: (state) => state.wallet,
  }),
  ...mapGetters('payments', ['latestTransaction', 'transactions']),
}

export const paymentsMethods = mapActions('payments', [
  'topUp',
  'fetchBalance',
  'verifyTransaction',
  'fetchTransactions',
  'cancelTopupTransaction',
])

export const notificationsComputed = {
  ...mapGetters('notifications', ['notifications', 'unseenNotifications']),
}

export const notificationsMethods = mapActions('notifications', [
  'fetchNotifications',
  'markNotificationsSeen',
])

export const skillsComputed = {
  ...mapGetters('skills', ['availableSkills']),
}
export const skillsMethods = mapActions('skills', ['fetchSkillsByListOfIDs'])

export const jobsMethods = mapActions('jobs', ['postJob', 'fetchJobs', 'applyProposal', 'haveIApplied', 'fetchMyJobs'])

export const jobsComputed = {
  ...mapGetters('jobs', ['feed', 'unapprovedJobs', 'myJobs']),
}
