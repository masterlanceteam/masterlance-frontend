import Vue from 'vue'
import router from '@router'
import store from '@state/store'
import CKEditor from '@ckeditor/ckeditor5-vue';
import AsyncComputed from 'vue-async-computed'

import App from './app.vue'
import vuetify from './plugins/vuetify'

// Globally register all `_base`-prefixed components
import '@components/_globals'

Vue.use(CKEditor);
Vue.use(AsyncComputed)
Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value.replace(/<\/p>/g, '``</p>')
  var text = div.textContent || div.innerText || "";

  return text.replace(/``/g, '<br/>');
});

// Don't warn about using the dev version of Vue in development.
Vue.config.productionTip = process.env.NODE_ENV === 'production'

// If running inside Cypress...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Ensure tests fail when Vue emits an error.
  Vue.config.errorHandler = window.Cypress.cy.onUncaughtException
}

const app = new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')

// If running e2e tests...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`.
  window.__app__ = app
}
