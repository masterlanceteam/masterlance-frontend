const moment = require('moment');

module.exports = {
  Message() {
      this.senderId = '';
      this.receiverId = '';
      this.type = '';
      this.content = '';
      this.analysis = {};
      this.time = moment();
  },
  MessageType: {
      AUDIO: 'audio',
      FILE: 'file', // pdf, images (restricted [.png, .jpg, ..]), .ppt/x ...
      TEXT: 'text'
  }
}
