import Vue from 'vue';
import io from 'socket.io-client';

// `${process.env.API_BASE_URL}:3004`
const socket = io(`http://20.77.1.64:3004`, {
  reconnectionDelayMax: 10000,
  auth: 'asdf'
});

Vue.use(socket);

export default socket
