// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify)

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#0F9D58',
        secondary: colors.grey.darken1,
        accent: colors.shades.black,
        error: '#db3236',
      },
      dark: {
        primary: colors.blue.darken1,
      },
    },
  },
}

export default new Vuetify(opts)
